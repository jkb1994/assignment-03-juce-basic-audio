/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes.
 
 This class processes the audio inputed, which is then filtered. The filtered signal is then sent to MainComponents.
 
 */
/** 

 
 
 */
#include "../../JuceLibraryCode/JuceHeader.h"
#include "SimpleFilter.h"
#include "dBFS.h"
#include <math.h>


class Audio : public AudioIODeviceCallback,
              public dBFS

{
public:
    /** Constructor*/
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    
    /**
     This is the function where all the audio work is done
     
     @param inputChannelData  = where all the audio data is accessed
     @param numInputChannels  = he number of input channels there are active
     @param outputchannelData = output audio which will be sent to the audio output device
     @param numOutputChannels = the number of output channels there are on the connected audio device
     @param numSamples        = match the size of the audio buffer specified on the interface to our application
     
     */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
 
    
    /** Gets called when audio device is about to start  */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /** Gets called when audio device has stopped  */
    void audioDeviceStopped() override;
    
    /** returns an array of processed audio for the ocatve filter which is then used in MainComponent to control the slider values. This is being called every 50ms in the timercallback function
     
     @param i = the number of values in the array
     @return = returns the an array of filtered data that is mused in mainComponent to control the slider values
     
     */
    
    float getocatveProcessedAudio(int i);
    /** returns an array of processed audio for the 1/2 ocatve filter which is then used in MainComponent to control the slider values. This is being called every 50ms in the timercallback function
     
     @parm i = the number of values in the array
     @return = returns the an array of filtered data that is mused in mainComponent to control the slider values
     */
    
    float gethalfOcatveProcessedAudio(int i);
   
    /** Returns the audio for the master input
     
     @return = returns the overall input level
     */
    float masterInput();
    
    /** Gets the ComboBox infomation from MainComponent and returns a value used to effect the decay  
     
     @param decayval = a value set when the comboBox has changed, which is then used to decide the amout of               decay. This is being updated every 50ms
     
     */
    float getDecayIn(float decayval);

private:
    AudioDeviceManager audioDeviceManager;
  
    
    static const int ocatveFilterNum = 9;
    SimpleFilter* ocatveBPFFilter[ocatveFilterNum];
    
    static const int halfOcatveFilterNum = 18;
    SimpleFilter* halfOcatveBPFFilter[halfOcatveFilterNum];
    
    
    float ocatveFilterdAudio[9];
    float ocatveProcessedAudio[9];
    
    float halfOcatveFilterdAudio[18];
    float halfOcatveProcessedAudio[18];
    float masterIn;
    
    int decayVal;
    
    double DB;
   
};



#endif  // AUDIO_H_INCLUDED
