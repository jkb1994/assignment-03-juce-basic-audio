/*
 *  SimpleFilter.h
 */



#ifndef __SimpleFilter_h__
#define __SimpleFilter_h__

/** Class were all the filtering is done.
 
 the class contains 4 types of filters:
 kLPF = Low Pass Filter
 kHPF = High Pass Filter
 kBPF = Band Pass Filter
 kBRF = Band Reject Filter
 
 */

#include <math.h>
#include "../../JuceLibraryCode/JuceHeader.h"

enum { kLPF = 0, kHPF, kBPF, kBRF, kNumberOfFilterTypes };

class SimpleFilter
{
public:
    /** Constructor.
    @param samplerate = recives the sample rate infomation and use's it in the filter
     */
    
	SimpleFilter (float samplerate);
    
    /** Sets the type of filter, the centre frequency and the band-width
     
    @param type = sets the tpye of Filter
     
    @param fc = sets the filters centre/cutoff frequency
     
    @param bw = sets the band-width(only for kBPF & kBRF)
     
     */
    
	void FilterConfig (int type, float fc, float bw);
	
    /** takes in the audio signal inside audioIODeviceCallback send it through the SimpleFilters class, which is processed by the decay value.
     
     @param sval = takes the audio signal and assigns it to a filter
     
     @return float = returns the filter audio
   
     */
    float Filter (float sval);
    
    
  
private:
	float fSampleRate;
	int iType;
	float fFilta[3], fFiltb[3], fFiltvalx[3], fFiltvaly[3];
    
    
};

#endif
