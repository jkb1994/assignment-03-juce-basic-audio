/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/



#include "Audio.h"

//==============================================================================
Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    // Constructs the filters for ocatve and 1/2 octave and sets the sampleRate
    for (int i = 0; i < ocatveFilterNum; i++) {
       ocatveBPFFilter[i] = new SimpleFilter(44100.f);
    }
    
    for (int i = 0; i < halfOcatveFilterNum; i++) {
        halfOcatveBPFFilter[i] = new SimpleFilter(44100.f);
    }
    
 
    // initialises audio values to 0
    for(int i = 0; i <= 8; i++)
    {
        ocatveProcessedAudio[i] = 0;
        ocatveFilterdAudio[i] = 0;
    }
    for(int i = 0; i <= 17; i++)
    {
        halfOcatveProcessedAudio[i] = 0;
        halfOcatveFilterdAudio[i] = 0;
    }
    
    masterIn = 0;
    
    // initialises decay value to 1 as its not possible to devide by 0
    decayVal = 1;
    
    // starts audio callback
    audioDeviceManager.addAudioCallback (this);

}
//==============================================================================
Audio::~Audio()
{
    // Ends audio callback
    audioDeviceManager.removeAudioCallback (this);
}

//==============================================================================
void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
    
    //Configure 9 channel filter to one ocatve 63Hz-22kHz
    ocatveBPFFilter[8]->FilterConfig(kBPF, 16000.0, 11360.0);
    ocatveBPFFilter[7]->FilterConfig(kBPF, 8000.0, 5680.0);
    ocatveBPFFilter[6]->FilterConfig(kBPF, 4000.0, 2840.0);
    ocatveBPFFilter[5]->FilterConfig(kBPF, 2000.0, 1420.0);
    ocatveBPFFilter[4]->FilterConfig(kBPF, 1000.0, 710.0);
    ocatveBPFFilter[3]->FilterConfig(kBPF, 500.0, 355.0);
    ocatveBPFFilter[2]->FilterConfig(kBPF, 250.0, 178.0);
    ocatveBPFFilter[1]->FilterConfig(kBPF, 125.0, 88.0);
    ocatveBPFFilter[0]->FilterConfig(kBPF, 63.0, 44.0);
    
    //Configure 18 channel filter to 1/2 octave 44Hz - 20kHz
    halfOcatveBPFFilter[17]->FilterConfig(kBPF, 16000.0, 5573.0);
    halfOcatveBPFFilter[16]->FilterConfig(kBPF, 11313.8, 3940.6);
    halfOcatveBPFFilter[15]->FilterConfig(kBPF, 8000.0, 2786.5);
    halfOcatveBPFFilter[14]->FilterConfig(kBPF, 5656.9, 1970.3);
    halfOcatveBPFFilter[13]->FilterConfig(kBPF, 4000.0, 1393.2);
    halfOcatveBPFFilter[12]->FilterConfig(kBPF, 2828.4, 985.2);
    halfOcatveBPFFilter[11]->FilterConfig(kBPF, 2000.0, 696.6);
    halfOcatveBPFFilter[10]->FilterConfig(kBPF, 1414.2, 492.6);
    halfOcatveBPFFilter[9]->FilterConfig(kBPF, 1000.0, 348.3);
    halfOcatveBPFFilter[8]->FilterConfig(kBPF, 707.1, 246.3);
    halfOcatveBPFFilter[7]->FilterConfig(kBPF, 500.0, 174.2);
    halfOcatveBPFFilter[6]->FilterConfig(kBPF, 353.6, 123.1);
    halfOcatveBPFFilter[5]->FilterConfig(kBPF, 250.0, 87.1);
    halfOcatveBPFFilter[4]->FilterConfig(kBPF, 176.8, 61.5);
    halfOcatveBPFFilter[3]->FilterConfig(kBPF, 125.0, 43.6);
    halfOcatveBPFFilter[2]->FilterConfig(kBPF, 88.4, 30.8);
    halfOcatveBPFFilter[1]->FilterConfig(kBPF, 62.5, 21.7);
    halfOcatveBPFFilter[0]->FilterConfig(kBPF, 44.2, 15.4);
 
}


//==============================================================================
void Audio::audioDeviceStopped()
{
    
}

//==============================================================================
void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
        //All audio processing is done here
        const float *inL = inputChannelData[0];
        float *outL = outputChannelData[0];
  

        float MicIn;
    
    // Filters audio inputfor ocatve & 1/2 octave filters
        while(numSamples--)
        {
            MicIn = *inL;
          
            for(int i = 0; i <= 8; i++)
                {
                    ocatveFilterdAudio[i] = ocatveBPFFilter[i]->Filter(MicIn);
                }
            
                for(int i = 0; i <= 17; i++)
                {
                    halfOcatveFilterdAudio[i] = halfOcatveBPFFilter[i]->Filter(MicIn);
                }
            
            inL++;
            outL++;
            
     }
  

    
    // Assigning the input into an array of 9, whilst decaying the signal for a controled slider response
    for (int i = 0; i <= 8; i++)
    {
        
        if ( ocatveProcessedAudio[i] < ocatveFilterdAudio[i])
        {
            ocatveProcessedAudio[i] = ocatveFilterdAudio[i];
        }
        else
        {
            ocatveProcessedAudio[i] -= ocatveProcessedAudio[i] / decayVal;
        }
    }
    
    // Assigning the input into an array of 18, whilst decaying the signal for a controled slider response
    for (int i = 0; i <= 17; i++)
    {
        
        if ( halfOcatveProcessedAudio[i] < halfOcatveFilterdAudio[i])
        {
            halfOcatveProcessedAudio[i] = halfOcatveFilterdAudio[i];
        }
        else
        {
           halfOcatveProcessedAudio[i] -= halfOcatveProcessedAudio[i] / decayVal;
        }
        
        
    }
        // Assigning the input the masterInput Slider.
        // whilst decaying the signal for a controled slider response
        if (masterIn < MicIn) {
            masterIn = MicIn;
        }
        else {
            masterIn -= masterIn/20;
        }
   
        // Converts the gain value into a dBFS value
        DB = gainToDB(masterIn);
   
    
}



//==============================================================================
float Audio::masterInput()
{
    
    
    return DB;
}

//==============================================================================
float Audio::getocatveProcessedAudio(int i)
{
   
    return ocatveProcessedAudio[i];
    
}
//==============================================================================
float Audio::gethalfOcatveProcessedAudio(int i)
{
    
    return halfOcatveProcessedAudio[i];
    
}

//==============================================================================
float Audio::getDecayIn(float decayval)
{
    
    if (decayval == 1)
    {
        decayVal = 10;
    }
    else if (decayval == 2)
    {
        decayVal = 20;
    }
    else if (decayval == 3)
    {
        decayVal = 100;
    }
    else if (decayval == 4)
    {
        decayVal = 1000;
    }
    
    
    return decayVal;
}



