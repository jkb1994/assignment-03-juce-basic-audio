//
//  dBFS.cpp
//  JuceBasicAudio
//
//  Created by Jacobs Laptop  on 25/01/2015.
//
//

#include "dBFS.h"

dBFS::dBFS()
{
    
}

dBFS::~dBFS()
{
    
}
// converts gain value to a dBFS value
 float dBFS::gainToDB(float gain)
{
    float db =  Decibels::gainToDecibels(gain);
   
    return db;
}
