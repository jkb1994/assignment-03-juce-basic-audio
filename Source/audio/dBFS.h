//
//  dBFS.h
//  JuceBasicAudio
//
//  Created by Jacobs Laptop  on 25/01/2015.
//
//

/**
 Converts a gain value into a dBFS value.
 */

#ifndef __JuceBasicAudio__dBFS__
#define __JuceBasicAudio__dBFS__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

class dBFS
{
public:
 
    /** Constructor*/
    dBFS();
    /** Deconstructor*/
    ~dBFS();
   
    
  /**
   Takes in the gain value and returns a dBFS value
   
   a gain value of 1.0 = 0dB
   
   */
 float gainToDB(float gain);
    
private:
  
};



#endif /* defined(__JuceBasicAudio__dBFS__) */
