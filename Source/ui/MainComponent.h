/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "LevelSliders.h"
#include "ControlBoxs.h"


/** Class is the top layer of interface. All the sliders, ComboBoxs and labels are made visible here.

 On the construction of this class timercallback is started, thus constantly updating the slider value.
 
 Audio is also constructed simultaneously.
 

 
 
 */




//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Timer,
                        public ComboBox::Listener,
                        public LevelSliders,
                        public ControlBoxs




{
public:
    //==============================================================================
    /** Constructor*/
    MainComponent (Audio& audio_);

    /** Destructor */
    ~MainComponent();

    /** Sets the size of the GUI objects*/
    void resized() override;
    
    /** Listens for call telling it the comboBox value has changes */
    void comboBoxChanged (ComboBox* comboBox) override;
    
    /** repaints the background of the screen*/
    void paint (Graphics& g) override;
   
    /** gets called everytime the timer changes state (50ms)*/ 
    void timerCallback();
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;

    
    
private:
    Audio& audio;

    Label halfOcatveSliderFrequency[18];
    Label octaveSliderFrequency[9];
    Label descriptions[3];
    
    float posLeft, poshight, width, hight;
    int sliderNum;
    int ocatveSliderNum;
    int halfOcatveSliderNum;
    
    int milSec;
    int resValue;
    int decayValue;

 

    //=======================================r=======================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
