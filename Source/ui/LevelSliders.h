//
//  LevelSliders.h
//  JuceBasicAudio
//
//  Created by Jacobs Laptop  on 25/01/2015.
//
//

/**
 Class creates multipul sliders to represent the frequency value
 */

#ifndef __JuceBasicAudio__LevelSliders__
#define __JuceBasicAudio__LevelSliders__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

class LevelSliders 
{
public:
    
    /** Constructor*/
    LevelSliders();
    
    /** Deconstuctor*/
    ~LevelSliders();
    

    /** create an array of sliders that are used inside MainComponent. there values being constantly updated with the getocatveProcessedAudio & getHalfOcatveProcessedAudio functions
     */
    Slider ocatveChannel[9];
    Slider halfOcatveChannel[18];
    Slider masterInput;
    
private:

};
#endif /* defined(__JuceBasicAudio__LevelSliders__) */
