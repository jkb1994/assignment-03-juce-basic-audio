//
//  ControlBoxs.cpp
//  JuceBasicAudio
//
//  Created by Jacobs Laptop  on 27/01/2015.
//
//

#include "ControlBoxs.h"

ControlBoxs::ControlBoxs()
{
    //Decay ComboBox
    decayComboBox.addItem("None", 1);
    decayComboBox.addItem("20%", 2);
    decayComboBox.addItem("100%", 3);
    decayComboBox.addItem("1000%", 4);
    decayComboBox.setSelectedId(1);
    
    //Resolution ComboBox
    resComboBox.addItem("Octave", 1);
    resComboBox.addItem("1/2 Octave", 2);
    resComboBox.setSelectedId(2);
}
ControlBoxs::~ControlBoxs()
{
    
}