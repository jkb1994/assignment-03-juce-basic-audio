//
//  LevelSliders.cpp
//  JuceBasicAudio
//
//  Created by Jacobs Laptop  on 25/01/2015.
//
//


#include "LevelSliders.h"

LevelSliders::LevelSliders()
{
    int i = 0;
    // Creates Sliders for octave filter representation
    for ( i = 0; i <= 8; i++)
    {
        ocatveChannel[i].setSliderStyle(Slider::LinearBarVertical);
        ocatveChannel[i].setRange(0.0, 1.0);
        ocatveChannel[i].setValue(0);
        halfOcatveChannel[i].setTextBoxStyle(Slider::NoTextBox , false, 20, 20);
        
    }
      // Creates Sliders for 1/2octave filter representation
    for ( i = 0; i <= 17; i++)
    {
        halfOcatveChannel[i].setSliderStyle(Slider::LinearBarVertical);
        halfOcatveChannel[i].setRange(0.0, 1.0);
        halfOcatveChannel[i].setValue(0);
        halfOcatveChannel[i].setTextBoxStyle(Slider::NoTextBox , false, 20, 20);
    }
    
    // creates slider for dBFS representation
    masterInput.setSliderStyle(Slider::LinearBarVertical);
    masterInput.setRange(-100.0, 0.0);
    masterInput.setValue(-100.0);
    masterInput.setTextBoxStyle(Slider::NoTextBox , false, 20, 20);
    masterInput.setSkewFactor(10.0);
}
LevelSliders::~LevelSliders()
{
    
}
