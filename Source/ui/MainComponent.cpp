/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"



//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    // sets the size of the window
    setSize (600, 300);
    
    // start timer
    
    // Makes the octave sliders visible
    for ( int i = 0; i <= 8; i++)
    {
       
        addAndMakeVisible(&ocatveChannel[i]);
    }
    // Makes the 1/2 octave sliders visible
    for (int i = 0; i <= 17; i++)
    {
        
        addAndMakeVisible(&halfOcatveChannel[i]);
    }
    // sets the labels
    octaveSliderFrequency[0].setText("62", dontSendNotification);
    octaveSliderFrequency[1].setText("125", dontSendNotification);
    octaveSliderFrequency[2].setText("250", dontSendNotification);
    octaveSliderFrequency[3].setText("500", dontSendNotification);
    octaveSliderFrequency[4].setText("1K", dontSendNotification);
    octaveSliderFrequency[5].setText("2K", dontSendNotification);
    octaveSliderFrequency[6].setText("4K", dontSendNotification);
    octaveSliderFrequency[7].setText("8K", dontSendNotification);
    octaveSliderFrequency[8].setText("16K", dontSendNotification);
    
    halfOcatveSliderFrequency[0].setText("44", dontSendNotification);
    halfOcatveSliderFrequency[1].setText("62", dontSendNotification);
    halfOcatveSliderFrequency[2].setText("88", dontSendNotification);
    halfOcatveSliderFrequency[3].setText("125", dontSendNotification);
    halfOcatveSliderFrequency[4].setText("176", dontSendNotification);
    halfOcatveSliderFrequency[5].setText("250", dontSendNotification);
    halfOcatveSliderFrequency[6].setText("353", dontSendNotification);
    halfOcatveSliderFrequency[7].setText("500", dontSendNotification);
    halfOcatveSliderFrequency[8].setText("707", dontSendNotification);
    halfOcatveSliderFrequency[9].setText("1K", dontSendNotification);
    halfOcatveSliderFrequency[10].setText("1.4K", dontSendNotification);
    halfOcatveSliderFrequency[11].setText("2K", dontSendNotification);
    halfOcatveSliderFrequency[12].setText("2.8K", dontSendNotification);
    halfOcatveSliderFrequency[13].setText("4K", dontSendNotification);
    halfOcatveSliderFrequency[14].setText("5.6K", dontSendNotification);
    halfOcatveSliderFrequency[15].setText("8k", dontSendNotification);
    halfOcatveSliderFrequency[16].setText("11.3K", dontSendNotification);
    halfOcatveSliderFrequency[17].setText("16K", dontSendNotification);
    
    descriptions[0].setText("INPUT", dontSendNotification);
    descriptions[1].setText("Resolution", dontSendNotification);
    descriptions[2].setText("Decay", dontSendNotification);
    

    // sets the font size, colour and makes visble the labels for the ocatve sliders
    for (int i = 0; i <= 8; i++)
    {
        
        octaveSliderFrequency[i].setFont(12);
        octaveSliderFrequency[i].setColour(Label::textColourId, Colours::antiquewhite);
        
        addAndMakeVisible(octaveSliderFrequency[i]);
        
    }
    
    // sets the font size, colour and makes visble the labels for the halfocatve sliders
    for (int i = 0; i <= 17; i++)
    {
    
        halfOcatveSliderFrequency[i].setFont(8);
        halfOcatveSliderFrequency[i].setColour(Label::textColourId, Colours::antiquewhite);
        addAndMakeVisible(halfOcatveSliderFrequency[i]);
    
    }
    // sets the font size, colour and makes visble the description labels
    for (int i = 0; i <= 2; i++)
    {
        descriptions[i].setFont(12);
        descriptions[i].setColour(Label::textColourId, Colours::antiquewhite);
        addAndMakeVisible(descriptions[i]);
    }

        // Makes the masterInput sliders visible
        addAndMakeVisible(&masterInput);
  
        // Makes Visible Decay ComboBox
        decayComboBox.addListener(this);
        addAndMakeVisible(&decayComboBox);
   
        //Makes Visible Resolution ComboBox
        resComboBox.addListener(this);
        addAndMakeVisible(&resComboBox);
    
        // start the timer class (50ms callback)
        startTimer(milSec);
    
        // initialises the number of sliders
        sliderNum = 0;
}

//==============================================================================
MainComponent::~MainComponent()
{

}
//==============================================================================
void MainComponent::resized()
{
   
    // sets the starting point of the slider position
    float octaveBounds = 100.0;
    float halfOcatveBounds = 100.0;

   
    // sets the bounds for the GUI's & Labels
    descriptions[0].setBounds(22, 20, 50, 20);
    descriptions[1].setBounds(175, 5, 50, 20);
    descriptions[2].setBounds(300, 5, 50, 20);
    
    
    masterInput.setBounds (25, 50, 30, 200);
    resComboBox.setBounds(175, 25, 100, 20);
    decayComboBox.setBounds(300, 25, 100, 20);
    
    
    if (sliderNum == 9)
    {
        for (int i = 0 ; i <= 8; i ++)
        {
            ocatveChannel[i].setBounds (octaveBounds + (i * 54), 50, 25, 200);
            octaveSliderFrequency[i].setBounds(octaveBounds + (i * 54), 250, 30, 20);
        
        }
        for (int i = 0; i <= 17; i++)
        {
            halfOcatveChannel[i].setBounds (0, 0, 0, 0);
            halfOcatveSliderFrequency[i].setBounds(0, 0, 0, 0);
        }

    }
    else if (sliderNum == 18)
    {
        for (int i = 0; i <= 17; i++)
        {
            
            halfOcatveChannel[i].setBounds (halfOcatveBounds + (i * 27), 50, 17, 200);
            halfOcatveSliderFrequency[i].setBounds(halfOcatveBounds + (i * 27), 250, 25, 20);
        }
        for (int i = 0; i <= 8; i++)
        {
           ocatveChannel[i].setBounds (0, 0, 0, 0);
           octaveSliderFrequency[i].setBounds(0, 0, 0, 0);
        }

    
    }

}
//==============================================================================
void MainComponent::comboBoxChanged(ComboBox *comboBox)
{
    
    // send a value when the comboBox has changed
    if (comboBox == &resComboBox)
    {
        if (comboBox->getSelectedId() == 1)
        {
           sliderNum = 9;
           
        }
        else if (comboBox->getSelectedId() == 2)
        {
           sliderNum = 18;
          
        }
        resized();
    }
   else if (comboBox == &decayComboBox)
   {
       
       decayValue = comboBox -> getSelectedId();
        std::cout<< "decay = "<< decayValue<< "\n";
  
   }
    
    
    
}
//==============================================================================
void MainComponent::paint (Graphics& g)
{
    g.fillAll();
}


//==============================================================================
void MainComponent::timerCallback()
{
    
    // retrives comboBox has changed value
    audio.getDecayIn(decayValue);
    
    // set master input value to master audio input
    masterInput.setValue(audio.masterInput());
    
    // sets slidervalues to the filtered audio signal
    for (int i = 0; i <= 8; i++)
    {
        ocatveChannel[i].setValue(audio.getocatveProcessedAudio(i));
       
    }
    for ( int i = 0; i <= 17; i++)
    {
        
        halfOcatveChannel[i].setValue(audio.gethalfOcatveProcessedAudio(i));
       
      
    }
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
//==============================================================================
//double MainComponent::getValue()
//{
//    float masterSliderValue = audio.masterInput();
//    
//    return masterSliderValue;
//}
//==============================================================================
