//
//  ControlBoxs.h
//  JuceBasicAudio
//
//  Created by Jacobs Laptop  on 27/01/2015.
//
//

/** class creates the comboBoxs used in MainComponent*/

#ifndef __JuceBasicAudio__ControlBoxs__
#define __JuceBasicAudio__ControlBoxs__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

class ControlBoxs
{

public:
    
    /**Constructor*/
    ControlBoxs();
    
    /**Deconstructor*/
    ~ControlBoxs();
    
    /** Creates Comboxs that are used to set the resolution value and the decay value
     
     
     
     */
    ComboBox resComboBox;
    ComboBox decayComboBox;
    
    
private:
    
};

#endif /* defined(__JuceBasicAudio__ControlBoxs__) */
