var searchData=
[
  ['gaintodb',['gainToDB',['../classd_b_f_s.html#a3373e78521be8fb206993101f4d04f58',1,'dBFS']]],
  ['getapplicationname',['getApplicationName',['../class_juce_application.html#a2317e5c478c85e404794fe9d772e9f71',1,'JuceApplication']]],
  ['getapplicationversion',['getApplicationVersion',['../class_juce_application.html#af1b372356801d3d5928e520ae675c04e',1,'JuceApplication']]],
  ['getaudiodevicemanager',['getAudioDeviceManager',['../class_audio.html#acfed7cef7687d5727643a3aeb810b7fa',1,'Audio']]],
  ['getdecayin',['getDecayIn',['../class_audio.html#abc2f8ef62cfb8c565b8a7cd90cbb3511',1,'Audio']]],
  ['gethalfocatveprocessedaudio',['gethalfOcatveProcessedAudio',['../class_audio.html#a4fc05063f8022c9e2f6b7efdfc4170a7',1,'Audio']]],
  ['getmenubarnames',['getMenuBarNames',['../class_main_component.html#abb3226917f6b1ec0fe420e9e837e0ab7',1,'MainComponent']]],
  ['getmenuforindex',['getMenuForIndex',['../class_main_component.html#aab76e7b9a141591f2c5dbdf94b0d7124',1,'MainComponent']]],
  ['getocatveprocessedaudio',['getocatveProcessedAudio',['../class_audio.html#af423f9c988a1fce6e24ff019be67a951',1,'Audio']]]
];
